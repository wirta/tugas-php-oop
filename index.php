<?php


    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');

    $sheep = new Animal("shaun");
    echo "Jenis Binatang = ".$sheep->name . "<br>";
    echo "Jumlah Kaki = ". $sheep->legs . "<br>";
    echo "Berdarah Dingin = ".$sheep->cold_blooded . "<br><br>";


    $kodok = new Frog("buduk");
    echo "Jenis Binatang = ".$kodok->name . "<br>";
    echo "Jumlah Kaki = ".$kodok->legs . "<br>";
    echo "Berdarah Dingin = ".$kodok->cold_blooded . "<br>";
    echo $kodok->jump() . "<br><br>"; 
    

    $sungokong = new Ape("Kera Sakti");
    echo "Jenis Binatang = ".$sungokong->name . "<br>";
    echo "Jumlah Kaki = ".$sungokong->legs . "<br>";
    echo "Berdarah Dingin = ".$sungokong->cold_blooded . "<br>";
    echo $sungokong->yell(); 

/*
    require_once('mobil.php');
    require_once('bus.php');
    $mobil = new Mobil("Avanza");
    echo "Nama Mobil = ".$mobil->name . "<br>";
    echo "Jumlah Roda = ".$mobil->roda . "<br>";
    echo "Bahan Bakar = ". $mobil->bahanbakar . "<br>";
    echo "Jumlah Spion = ". $mobil->spion . "<br><br>";
    
    $bus = new Bus("Mini Bus");
    echo "Nama Mobil = ".$bus->name . "<br>";
    echo "Jumlah Roda = ".$bus->roda . "<br>";
    echo "Bahan Bakar = ". $bus->bahanbakar . "<br>";
    echo "Jumlah Spion = ". $bus->spion . "<br>";
    echo $bus->Jalan();  

*/




?>
